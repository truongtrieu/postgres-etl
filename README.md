# The purpose of this project
Collect and transform the raw data to create a new data formation that will help analyst team can use the data for their goal of analysis

# Structure of schemma and it's meaning
The new schemma database will has 1 Fact table (songplay) and 4 Dimension tables (songs, artists, user, time)
The database is designed follow to start style with detail:
- songplay (Fact): contains the informations of user when play a song 
- songs (Dimension): contains the informations about the songs like song id, artist id, song name, time, ect.
- artists (Dimension): contains the informations about artists such as artist id, artist name, artist location, ect.
- time (Dimension): contains the informations are extracted from timestamp
- user(Dimension): contains the the informations about user such as user name, level, gender, ect.

The purpose of this schemma is to help analyst team can easy to find the relationship between those tables then they can use exact query to collect data

# ETL pipeline
The ETL pipline workflows include:
- Step 1: Extract data from song data into users and songs data then insert appropriate data into those tables
- Step 2: Extract data from log data, filter by NextSong action
- Step 3: Extract timestamp into different type of time then inport appropriate data into time table
- Step 4: import appropriate data from step 2 into user table
- Step 5: Collect song id and artist id by join song and artist table then collaborate with data from step 2 to import into songplay table

# Explaination of the files
- sql_queries.sql: collection all postgressql query for using through all this project
- create_tables.py: this file is used to create database and reset the tables with DROP and CREATE action 
- etl.py: This file used to process to extract json file, collect data, transform data and input data into new schemma
- etl.ipynd: This file go along with each step to help resolve each story with hint
- test.ipynb: used to test result of importing data by single query on each table